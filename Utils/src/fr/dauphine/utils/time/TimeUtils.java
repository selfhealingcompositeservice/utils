package fr.dauphine.utils.time;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
	
	
	public static Timestamp stringToTimestamp(String datestring, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date  date = null;
		try {
			date = dateFormat.parse(datestring);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new Timestamp(date.getTime());
	}

}
