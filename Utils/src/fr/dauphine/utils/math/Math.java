package fr.dauphine.utils.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Math {
	
	public static int randInt(int min, int max) {

	    Random rand = new Random();
	    return rand.nextInt((max - min) + 1) + min;
	}
	
	public static double randDouble(double min, double max) {

	    Random rand = new Random();
	    return min + (max - min) * rand.nextDouble();
	}
	
	//http://www.javamex.com/tutorials/random_numbers/gaussian_distribution_2.shtml
	//to generate values with an average of avg and a standard deviation of std, we call:
	public static List<Double> getGaussianList(int size, double avg, double std) {
		List<Double> values = new ArrayList<Double>();
		
		Random r = new Random();
		
		while(values.size() < size) {
			double val = r.nextGaussian() * std + avg;
			if(val > 0)
				values.add(val);
		}
	
		return values;
	}

	
	
	public static void main(String...args) {
	
			//System.out.println(randDouble(0.6, 1.0));
			System.out.println(getGaussianList(1000, 4.05, 17.32).size());
	}

}