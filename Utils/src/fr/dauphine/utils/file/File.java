package fr.dauphine.utils.file;

import java.io.IOException;

import com.google.common.base.Charsets;
import com.google.common.io.Files;


public class File {

	public static void write(String filename, String data) {
		try {
			Files.append(data + "\n", new java.io.File(filename), Charsets.UTF_8 );
		} catch( IOException e ) {
			e.printStackTrace();
		}
	}
}
